<?php

namespace Eco\Utils;

class Arrays
{

    /**
     * Merge $config1 & $config2
     *
     * @param array $config1
     * @param array $config2
     * @throws \RuntimeException
     * @return array
     */
    public static function merge(array $config1, array $config2)
    {
        foreach ($config2 as $key => $value) {
            if (!is_string($key)) {
                return array_merge($config1, $config2); // Assume an indexed array
            }

            if (!\array_key_exists($key, $config1)) {
                $config1[$key] = $value;
                continue;
            }

            if (is_array($value)) {
                if (!is_array($config1[$key])) {
                    throw new \RuntimeException("Unable to merge an array with a non array");
                }

                $config1[$key] = self::merge($config1[$key], $value);
                continue;
            }

            $config1[$key] = $value;
        }

        return $config1;
    }

}
