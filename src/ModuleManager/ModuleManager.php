<?php

namespace Eco\ModuleManager;

use Eco\Utils\Arrays;

class ModuleManager
{

    /**
     * Modules chargés
     *
     * @var array
     */
    protected $modules = [];

    /**
     * Configuration issue des modules
     *
     * @var array|null
     */
    protected $modulesConfig;

    /**
     * Modules initialisés
     *
     * @var array
     */
    protected $initializedModules = [];

    /**
     * Namespace de chaque modules
     * 
     * @var array
     */
    protected $modulesNamespaces = [];

    /**
     * ModuleManager constructor.
     *
     * @param array $modules
     */
    public function __construct(array $modules = [])
    {
        $this->addModules($modules);
    }

    /**
     * Add modules
     *
     * @param array $modules
     * @return $this
     */
    public function addModules(array $modules)
    {
        foreach ($modules as $moduleName) {
            $this->addModule($moduleName);
        }

        return $this;
    }

    /**
     * Instantiate the module class
     *
     * @param string $module
     * @throws \RuntimeException
     * @return ModuleManager
     */
    public function addModule($module)
    {
        if (is_string($module)) {
            $moduleClass = $module;
            $moduleInstance = new $moduleClass;
        } else {
            $moduleClass = get_class($module);
            $moduleInstance = $module;
        }

        if (!$moduleInstance instanceof ModuleInterface) {
            throw new \RuntimeException("Module instance of '$moduleClass' does not implement ModuleInterface");
        }

        $moduleName = $moduleInstance->getName();

        if (array_key_exists($moduleName, $this->modules)) {
            throw new \RuntimeException("Can't add a module instance with the same name as another registered module");
        }

        $this->modulesConfig = null;
        $this->modules[$moduleName] = $moduleInstance;

        $pos = strrpos($moduleClass, '\\');
        if ($pos === false) {
            $this->modulesNamespaces[$moduleName] = '';
        } else {
            $this->modulesNamespaces[$moduleName] = substr($moduleClass, 0, $pos);
        }

        return $this;
    }

    /**
     * Retourne la configuration fusionnées issue des modules
     *
     * @return array
     */
    public function getConfig()
    {
        if ($this->modulesConfig === null) {
            $moduleConfig = [];
            foreach ($this->modules as $moduleName => $moduleInstance) {
                $moduleConfig = Arrays::merge($moduleConfig, $moduleInstance->getConfig());
            }

            $this->modulesConfig = $moduleConfig;
        }

        return $this->modulesConfig;
    }

    /**
     * Retourne les instances des modules chargés
     *
     * @return ModuleInterface[]
     */
    public function getModules()
    {
        return $this->modules;
    }

    /**
     * Retourne l'instance d'un module donné
     *
     * @param string $name
     * @return ModuleInterface
     */
    public function getModule($name)
    {
        return $this->modules[$name];
    }

    /**
     * Retourne le namespace d'un module donné
     * 
     * @param string $name
     * @return string
     */
    public function getModuleNamespace($name)
    {
        return $this->modulesNamespaces[$name];
    }
}
