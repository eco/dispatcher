<?php

namespace Eco\ModuleManager;

interface ModuleInterface
{
    public function getName();
    public function getConfig();
    public function getPath();
}
