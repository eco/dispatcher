<?php

namespace Eco\ViewEngine;

use Psr\Http\Message\ResponseInterface;

interface ViewEngineInterface
{
    public function render($template, $params): ResponseInterface;
}