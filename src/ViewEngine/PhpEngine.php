<?php

namespace Eco\ViewEngine;

use Psr\Http\Message\ResponseInterface;
use Eco\ModuleManager\ModuleInterface;
use Zend\Diactoros\Response\HtmlResponse;

class PhpEngine implements ViewEngineInterface
{
    /**
     * @var ModuleInterface[]
     */
    protected $modules;

    public function __construct($modules)
    {
        $this->modules = $modules;
    }

    /**
     * @param string $template
     * @return array
     */
    protected function getModuleAndPathFromTemplate(string $template)
    {
        $pos = strpos($template, '/');
        if ($pos === false || $pos === 0) {
            throw new \RuntimeException("No module");
        }

        return [
            'module' => substr($template, 0, $pos),
            'path'   => substr($template, $pos + 1)
        ];
    }

    public function render($template, $params): ResponseInterface
    {
        $___moduleNameAndPath = $this->getModuleAndPathFromTemplate($template);

        // Get View path
        $___viewPath = $this->modules[$___moduleNameAndPath['module']]
            . '/views/'
            . $___moduleNameAndPath['path']
            . '.phtml';

        // array to vars
        extract($params);

        // Http Headers
        $httpHeaders = [];

        // Http Status
        $httpStatus = 200;

        ob_start();
        try {
            require $___viewPath;
        } catch (\Throwable $e) {
            ob_end_clean();
            throw $e;
        }

        return new HtmlResponse(
            ob_get_clean(),
            $httpStatus,
            $httpHeaders
        );
    }

}