<?php

namespace Eco\Router;

class RouterConfig extends Router
{
    public function __construct(array $routerConfig, array $options = [])
    {
        parent::__construct($options);

        $this->configRoutes($routerConfig['routes'], $routerConfig['defaults']);
    }

    protected function getDefaults($route, $defaults)
    {
        $newDefaults = $defaults;
        if (isset($route['module'])) {
            $newDefaults['module'] = $route['module'];
        }
        if (isset($route['controller'])) {
            $newDefaults['controller'] = $route['controller'];
        }
        if (isset($route['action'])) {
            $newDefaults['action'] = $route['action'];
        }
        return $newDefaults;
    }

    protected function configGroup($group, $default)
    {
        $this->startGroup($group['prefix']);
        $this->configRoutes($group['routes'], $default);
        $this->endGroup();
    }

    protected function configRoutes($routes, $defaults)
    {
        foreach ($routes as $route) {
            $newDefaults = $this->getDefaults($route, $defaults);

            if (isset($route['prefix'])) {
                // This is a group !
                $this->configGroup($route, $newDefaults);
            } else if (isset($route['route'])) {
                // Standard route
                $this->addRoute(
                    isset($route['method']) ? $route['method'] : 'GET',
                    $route['route'],
                    $newDefaults
                );
            } else {
                throw new \RuntimeException("A route should have either a route or a prefix key");
            }
        }
    }
}