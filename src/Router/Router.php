<?php

namespace Eco\Router;

use FastRoute\DataGenerator\GroupCountBased;
use FastRoute\Dispatcher\GroupCountBased as GcbDispatcher;
use FastRoute\RouteCollector;
use FastRoute\RouteParser\Std;

class Router extends RouteCollector
{
    /**
     * @var bool|string
     */
    protected $dispatcherClass;

    /**
     * Previous prefixes
     *
     * @var array[string]
     */
    protected $previousPrefixes = [];


    public function __construct(array $options = [])
    {
        $routeParser = isset($options['routeParser']) ?: Std::class;
        $dataGenerator = isset($options['dataGenerator']) ?: GroupCountBased::class;
        $this->dispatcherClass = isset($options['dispatcher']) ?: GcbDispatcher::class;

        parent::__construct(
            new $routeParser,
            new $dataGenerator
        );
    }

    public function startGroup($prefix)
    {
        array_push($this->previousPrefixes, $this->currentGroupPrefix);
        $this->currentGroupPrefix .= $prefix;
    }

    public function endGroup()
    {
        $this->currentGroupPrefix = array_pop($this->previousPrefixes);
    }

    public function dispatch($method, $uri)
    {
        $dispatcher = new $this->dispatcherClass(
            $this->getData()
        );

        return $dispatcher->dispatch($method, $uri);
    }
}
