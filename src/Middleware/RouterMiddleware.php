<?php

namespace Eco\Middleware;

use FastRoute\Dispatcher;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Eco\Router\RouterConfig;
use Psr\Http\Server\RequestHandlerInterface;

class RouterMiddleware
{
    /**
     * @var RouterConfig
     */
    protected $router;

    /**
     * @var string[]
     */
    protected $notFoundRoute;

    /**
     * @var string[]
     */
    protected $errorRoute;

    /**
     * @var string[]
     */
    protected $notAllowedRoute;

    public function __construct(array $routerConfig)
    {
        // TODO : ajouter les modules possibles
        $this->router = new RouterConfig($routerConfig);

        $this->notFoundRoute   = $routerConfig['NotFoundRoute'];
        $this->errorRoute      = $routerConfig['ErrorRoute'];
        $this->notAllowedRoute = $routerConfig['NotAllowedRoute'];
    }

    public function __invoke(ServerRequestInterface $request, RequestHandlerInterface $next): ResponseInterface
    {
        $routable = str_replace(
            web_root(),
            '/',
            $request->getUri()->getPath()
        );

        $routeInfo = $this->router->dispatch(
            $request->getMethod(),
            $routable
        );

        switch ($routeInfo[0]) {
            default:
            case Dispatcher::NOT_FOUND:
                $route = $this->notFoundRoute;
                $routeParams = [
                    'path' => $request->getUri()->getPath(),
                    'method' => $request->getMethod()
                ];
                break;

            case Dispatcher::METHOD_NOT_ALLOWED:
                $route = $this->notAllowedRoute;
                $routeParams = [
                    'path' => $request->getUri()->getPath(),
                    'method' => $request->getMethod()
                ];
                break;

            case Dispatcher::FOUND:
                $route = $routeInfo[1];
                $routeParams = $routeInfo[2];
                break;
        }

        return $next->handle(
            $request->withAttribute(
                'route',
                array_merge($route, $routeParams)
            )
        );
    }
}