<?php

namespace Eco\Middleware;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Eco\ViewEngine\ViewEngineInterface;
use Eco\ViewEngine\ViewEngineResult;
use Psr\Http\Server\RequestHandlerInterface;

class LayoutMiddleware
{
    protected $engine;
    protected $layout;

    public function __construct(ViewEngineInterface $engine, array $layout)
    {
        $this->engine = $engine;
        $this->layout = $layout;
    }

    public function __invoke(ServerRequestInterface $request, RequestHandlerInterface $next): ResponseInterface
    {
        $response = $request->getAttribute('response');

        // TODO : find a way to pass lang & title
        // TODO : fix webRoot
        $response = $this->engine
            ->render( $this->layout['module'] . '/' . $this->layout['path'],
                [
                    'title'    => $request->getAttribute('title', 'Titre'),
                    'lang'     => $request->getAttribute('lang', 'fr'),
                    'content'  => $response->getBody()->getContents(),
                    'webRoot'  => $request->getAttribute('web_root')
                ]
            );

        return $response;
    }
}
