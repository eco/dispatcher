<?php

namespace Eco\Middleware;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;

class DispatchMiddleware
{
    protected static function normalize(string $string)
    {
        return preg_replace_callback(
            '|([-_])([a-z])|',
            function ($what) {
                if ($what[1]=='_') {
                    return '_' . strtoupper($what[2]);
                }
                return strtoupper($what[2]);
            },
            strtolower($string)
        );
    }

    protected static function getControllerClass($route)
    {
        return
            ucfirst(self::normalize($route['module']))
            . '\\Controller\\'
            . ucfirst(self::normalize($route['controller']))
            . 'Controller';
    }

    protected static function getActionMethod($route)
    {
        return self::normalize($route['action']) . 'Action';
    }

    protected static function getParams($route)
    {
        unset($route['module']);
        unset($route['controller']);
        unset($route['action']);

        return $route;
    }

    public function __invoke(ServerRequestInterface $request, RequestHandlerInterface $next): ResponseInterface
    {
        $route = $request->getAttribute('route');
        $controllerClass = self::getControllerClass($route);
        $actionMethod = self::getActionMethod($route);
        $params = self::getParams($route);

        $controller = new $controllerClass();
        $result = call_user_func_array([$controller, $actionMethod], $params);

        return $next->handle(
            $request->withAttribute(
                'result',
                $result
            )
        );
    }
}