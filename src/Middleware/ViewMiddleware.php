<?php

namespace Eco\Middleware;

use Eco\ViewEngine\ViewEngineInterface;
use Eco\ViewEngine\ViewEngineResult;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Zend\Diactoros\Response\EmptyResponse;

class ViewMiddleware
{
    protected $engine;

    public function __construct(ViewEngineInterface $engine)
    {
        $this->engine = $engine;
    }

    public function __invoke(ServerRequestInterface $request, RequestHandlerInterface $next): ResponseInterface
    {
        $result = $request->getAttribute('result');

        if ($result === false) {
            return new EmptyResponse();
        }

        if ($result instanceof ResponseInterface) {
            return $result;
        }

        // TODO : fix webRoot
        $route = $request->getAttribute('route');
        $response = $this->engine
            ->render(
                $route['module']
                . '/' . $route['controller']
                . '/' . $route['action'],
                ['webRoot' => $request->getAttribute('web_root') ] + $result
            );

        return $next->handle(
            $request
                ->withAttribute('response', $response)
        );
    }
}
