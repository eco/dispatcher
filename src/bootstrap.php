<?php
if (!defined('APP_IS_DEV')) {
    define('APP_IS_DEV', false);
}

if (!defined('APP_DIR')) {
    throw new \RuntimeException("APP_DIR must be defined");
}

if (!defined('PUBLIC_DIR')) {
    define('PUBLIC_DIR', APP_DIR . '/public');
}

if (!defined('DATA_DIR')) {
    define('DATA_DIR', APP_DIR . '/data');
}

set_error_handler(function($errno, $errstr, $errfile, $errline, array $errcontext) {
    throw new ErrorException($errstr, 0, $errno, $errfile, $errline);
});

// View functions
function e($vars) {
    foreach (func_get_args() as $arg) {
        echo htmlentities($arg, ENT_NOQUOTES, "UTF-8");
    }
}

// web root
function web_root() {
    static $webRoot;
    if (! $webRoot) {
        $webRoot = dirname(str_replace($_SERVER['DOCUMENT_ROOT'], '', $_SERVER['SCRIPT_FILENAME']));
        if (strlen($webRoot)) {
            $webRoot .= '/';
        }
    }
    return $webRoot;
}
