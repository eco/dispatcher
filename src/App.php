<?php

namespace Eco;

use Middlewares\Whoops;
use mindplay\middleman\Dispatcher;
use Psr\Http\Message\ServerRequestInterface;
use Eco\Middleware\DispatchMiddleware;
use Eco\Middleware\LayoutMiddleware;
use Eco\Middleware\RouterMiddleware;
use Eco\Middleware\ViewMiddleware;
use Eco\ModuleManager\ModuleManager;
use Eco\ViewEngine\PhpEngine;

require_once __DIR__ . "/bootstrap.php";

class App
{

    protected $modules;

    /**
     * @var array
     */
    protected $config;

    public function __construct(array $modules)
    {
        // Load modules
        $moduleManager = new ModuleManager($modules);

        // Get modules names and paths from MM
        foreach ($moduleManager->getModules() as $moduleName => $module) {
            $this->modules [$moduleName] = $module->getPath();
        }

        // Config from modules
        $this->config = $moduleManager->getConfig();
    }

    public function getModules()
    {
        return $this->modules;
    }

    public function getConfig()
    {
        return $this->config;
    }

    public function dispatch(ServerRequestInterface $request)
    {
        // Middleman dispatcher config
        $dispatcher = new Dispatcher([
            function (ServerRequestInterface $sr, $next) {
                return (new Whoops())->process($sr, $next);
            },
            new RouterMiddleware($this->config['router']),
            new DispatchMiddleware(),
            new ViewMiddleware(
                new PhpEngine($this->modules)
            ),
            new LayoutMiddleware(
                new PhpEngine($this->modules),
                $this->config['layout']
            )
        ]);

        return $dispatcher->dispatch($request);
    }
}