<?php

namespace Eco\ModuleManager\Test;

use Mockery\Adapter\Phpunit\MockeryTestCase;
use Eco\ModuleManager\ModuleInterface;
use Eco\ModuleManager\ModuleManager;

class TestModule implements ModuleInterface
{
    public function getName() { return 'test'; }
    public function getConfig() { return []; }
    public function getPath() { return __DIR__; }
}

class TestModule2 implements ModuleInterface
{
    public function getName() { return 'test2'; }
    public function getConfig() { return []; }
    public function getPath() { return __DIR__; }
}

class ModuleManagerTest extends MockeryTestCase
{

    /**
     * @var ModuleManager
     */
    protected $moduleManager;

    protected function setUp()
    {
        $this->moduleManager = new ModuleManager();
    }

    protected function tearDown()
    {
        $this->moduleManager = null;
    }

    public function testAddModule()
    {
        $this->moduleManager->addModule(TestModule::class);

        $this->assertArrayHasKey('test', $this->moduleManager->getModules());
        $this->assertInstanceOf(TestModule::class, $this->moduleManager->getModule('test'));
    }

    public function testAddModuleInstance()
    {
        $this->moduleManager->addModule(new TestModule());

        $this->assertArrayHasKey('test', $this->moduleManager->getModules());
        $this->assertInstanceOf(TestModule::class, $this->moduleManager->getModule('test'));
    }

    public function testAddModuleInstanceThrowException()
    {
        $this->expectException(\RuntimeException::class);

        $this->moduleManager->addModule(new TestModule());
        $this->moduleManager->addModule(new TestModule());
    }

    public function testAddModules()
    {
        $modules = [
            TestModule::class,
            TestModule2::class
        ];

        $this->moduleManager->addModules($modules);

        $this->assertArrayHasKey('test', $this->moduleManager->getModules());
        $this->assertInstanceOf(TestModule::class, $this->moduleManager->getModule('test'));
        $this->assertArrayHasKey('test2', $this->moduleManager->getModules());
        $this->assertInstanceOf(TestModule2::class, $this->moduleManager->getModule('test2'));
    }

    public function testGetNamespace()
    {
        $this->moduleManager->addModule(TestModule::class);

        $moduleNs = $this->moduleManager->getModuleNamespace('test');

        $this->assertSame('Eco\ModuleManager\Test', $moduleNs);
    }

    public function testGetEmptyNamespace()
    {
        $moduleMock = \Mockery::mock(\Eco\ModuleManager\ModuleInterface::class); // => a non-namespaced class is generated
        $moduleMock->shouldReceive('getName')->once()->andReturn('test');

        $this->moduleManager->addModule($moduleMock);

        $moduleNs = $this->moduleManager->getModuleNamespace('test');

        $this->assertSame('', $moduleNs);
    }

    protected function getModuleInstanceMockForGetConfig($name, array $config)
    {
        $moduleMock = \Mockery::mock(ModuleInterface::class);
        $moduleMock->shouldReceive('getName')
            ->andReturn($name);
        $moduleMock->shouldReceive('getConfig')
            ->andReturn($config);

        return $moduleMock;
    }

    public function testGetModulesConfig()
    {
        $moduleMock = $this->getModuleInstanceMockForGetConfig('test', array(
            'var' => 'value',
        ));
        $this->moduleManager->addModule($moduleMock);

        $moduleMock = $this->getModuleInstanceMockForGetConfig('test2', array(
            'var2' => 'value2',
        ));
        $this->moduleManager->addModule($moduleMock);

        $config = $this->moduleManager->getConfig();

        $this->assertInternalType('array', $config);
        $this->assertArrayHasKey('var', $config);
        $this->assertArrayHasKey('var2', $config);
        $this->assertCount(2, $config);
    }

    public function testGetModuleConfigWithDefault()
    {
        $test = $this->getModuleInstanceMockForGetConfig('test', array(
            'first' => 'test'
        ));
        $test2 = $this->getModuleInstanceMockForGetConfig('test2', array(
            'first' => 'test2'
        ));

        $this->moduleManager->addModule($test, false);
        $this->moduleManager->addModule($test2, true);

        $config = $this->moduleManager->getConfig();

        $this->assertSame('test2', $config['first']);
    }

    public function testFailIfNotModuleInterfaceInstanceIsAdded()
    {
        $this->expectException(\RuntimeException::class);

        $this->moduleManager->addModule(new \stdClass());
    }

}
