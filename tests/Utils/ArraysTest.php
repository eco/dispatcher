<?php

namespace Eco\Utils\Test;

use PHPUnit\Framework\TestCase;
use Eco\Utils\Arrays;

class ArraysTest extends TestCase
{

    public function testMerge()
    {
        $array1 = array(
            'var' => 'value',
        );
        $array2 = array(
            'var' => 'value2',
            'var2' => 'value',
        );

        $config = Arrays::merge($array1, $array2);

        $this->assertArrayHasKey('var', $config);
        $this->assertArrayHasKey('var2', $config);
        $this->assertEquals('value2', $config['var']);
        $this->assertEquals('value', $config['var2']);
    }

    public function testMergeRecursive()
    {
        $array1 = array(
            'var' => array('value'),
            'var3' => array(
                'key' => 'value',
            ),
            'var4' => array(
                'key' => array(
                    'key' => 'value',
                ),
                'key2' => array(
                    'value'
                )
            )
        );
        $array2 = array(
            'var' => array('value2'),
            'var2' => 'value',
            'var3' => array(
                'key' => 'value2',
                'key2' => 'value',
            ),
            'var4' => array(
                'key' => array(
                    'key' => 'value2',
                ),
                'key2' => array(
                    'value2'
                )
            )
        );

        $config = Arrays::merge($array1, $array2);

        $this->assertInternalType('array', $config);
        $this->assertInternalType('array', $config['var']);
        $this->assertContains('value', $config['var']);
        $this->assertContains('value2', $config['var']);
        $this->assertSame('value', $config['var2']);
        $this->assertInternalType('array', $config['var3']);
        $this->assertSame('value2', $config['var3']['key']);
        $this->assertSame('value', $config['var3']['key2']);
        $this->assertInternalType('array', $config['var4']);
        $this->assertInternalType('array', $config['var4']['key']);
        $this->assertSame('value2', $config['var4']['key']['key']);
        $this->assertInternalType('array', $config['var4']['key2']);
        $this->assertContains('value', $config['var4']['key2']);
        $this->assertContains('value2', $config['var4']['key2']);
    }

    public function testMergeMustThrowsException()
    {
        $this->expectException(\RuntimeException::class);

        $array1 = array(
            'var' => 'value',
        );
        $array2 = array(
            'var' => array('value2'),
        );

        $config = Arrays::merge($array1, $array2);
    }

}
