<?php

namespace Eco\Middleware\Test;

use Eco\Middleware\LayoutMiddleware;
use Mockery\Adapter\Phpunit\MockeryTestCase;
use Zend\Diactoros\Response;

class LayoutMiddlewareTest extends MockeryTestCase
{
    protected $request,
        $next,
        $response,
        $engine;

    public function setUp()
    {
        $this->request = \Mockery::mock('Psr\Http\Message\ServerRequestInterface');
        $this->next = \Mockery::mock('Psr\Http\Server\RequestHandlerInterface');
        $this->engine = \Mockery::mock('Eco\ViewEngine\ViewEngineInterface');
    }

    public function tearDown()
    {
        \Mockery::close();
    }

    public function testProcessWithCraftedResponse()
    {
        $vResp = new Response\HtmlResponse('hey ya !');
        $lResp = new Response\TextResponse('Hey ya !');

        $this->request->shouldReceive('getAttribute')
            ->withArgs(['response'])
            ->andReturn($vResp);

        $this->request->shouldReceive('getAttribute')
            ->withArgs(['title', 'Titre'])
            ->andReturn('title');

        $this->request->shouldReceive('getAttribute')
            ->withArgs(['lang', 'fr'])
            ->andReturn('fr');

        $this->request->shouldReceive('getAttribute')
            ->withArgs(['web_root'])
            ->andReturn('/');


        $this->engine->shouldReceive('render')
            ->withArgs(['test/path', [
                'title' => 'title',
                'lang' => 'fr',
                'content' => 'hey ya !',
                'webRoot' => '/',
            ]])
            ->andReturn($lResp);

        $lm = new LayoutMiddleware(
            $this->engine,
            ['module' => 'test', 'path' => 'path']
        );

        $result = $lm->process($this->request, $this->next);

        $this->assertInstanceOf('Psr\Http\Message\ResponseInterface', $result);
        $this->assertEquals($result, $lResp);
    }
/*
    public function testProcess()
    {
        $this->request->shouldReceive('getAttribute')
            ->withArgs(['result'])
            ->andReturn(['content' => 'value']);

        $this->request->shouldReceive('getAttribute')
            ->withArgs(['route'])
            ->andReturn([
                'module' => 'test',
                'controller' => 'ctrl',
                'action' => 'act'
            ]);

        $this->request->shouldReceive('getAttribute')
            ->withArgs(['web_root'])
            ->andReturn('/');

        $response = new Response\TextResponse('hey ya !');
        $this->engine->shouldReceive('render')
            ->withArgs(['test/ctrl/act', ['webRoot' => '/', 'content' => 'value']])
            ->andReturn($response);

        $this->request->shouldReceive('withAttribute')
            ->withArgs(['response', $response])
            ->andReturn($this->request);

        $this->next->shouldReceive('handle')
            ->withArgs([$this->request])
            ->andReturn($response);

        $lm = new LayoutMiddleware($this->engine);
        $result = $lm->process($this->request, $this->next);

        $this->assertInstanceOf('Psr\Http\Message\ResponseInterface', $result);
        $this->assertEquals($result, $response);
    }*/
}
