<?php
namespace Test\Controller {
    class CtrlController {
        public function actAction() {
            return 'some result';
        }
    }
}

namespace Eco\Middleware\Test {

    use Eco\Middleware\DispatchMiddleware;
    use PHPUnit\Framework\TestCase;

    class DispatchMiddlewareTest extends TestCase
    {
        public function tearDown()
        {
            \Mockery::close();
        }

        public function testProcess()
        {
            $request = \Mockery::mock('Psr\Http\Message\ServerRequestInterface');
            $next = \Mockery::mock('Psr\Http\Server\RequestHandlerInterface');
            $response = \Mockery::mock('Psr\Http\Message\ResponseInterface');

            $request->shouldReceive('getAttribute')
                ->withArgs(['route'])
                ->andReturn([
                    'module' => 'test',
                    'controller' => 'ctrl',
                    'action' => 'act',
                ]);

            $request->shouldReceive('withAttribute')
                ->withArgs(['result', 'some result'])
                ->andReturn($request);
            $next->shouldReceive('handle')
                ->withArgs([$request])
                ->andReturn($response);

            $dm = new DispatchMiddleware();
            $result = $dm->process($request, $next);

            $this->assertInstanceOf('Psr\Http\Message\ResponseInterface', $result);
        }
    }
}