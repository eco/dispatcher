<?php

namespace Eco\Middleware\Test;

use Eco\Middleware\ViewMiddleware;
use Mockery\Adapter\Phpunit\MockeryTestCase;
use Zend\Diactoros\Response;

class ViewMiddlewareTest extends MockeryTestCase
{
    protected $request,
        $next,
        $response,
        $engine;

    public function setUp()
    {
        $this->request = \Mockery::mock('Psr\Http\Message\ServerRequestInterface');
        $this->next = \Mockery::mock('Psr\Http\Server\RequestHandlerInterface');
        $this->engine = \Mockery::mock('Eco\ViewEngine\ViewEngineInterface');
    }

    public function testProcessWithFalse()
    {
        $this->request->shouldReceive('getAttribute')
            ->withArgs(['result'])
            ->andReturn(false);

        $vm = new ViewMiddleware($this->engine);

        $response = $vm->process($this->request, $this->next);

        $this->assertInstanceOf('Zend\Diactoros\Response\EmptyResponse', $response);
    }

    public function testProcessWithAlreadyCraftedResponse()
    {
        $response = new Response\TextResponse('hey ya !');

        $this->request->shouldReceive('getAttribute')
            ->withArgs(['result'])
            ->andReturn($response);

        $vm = new ViewMiddleware($this->engine);

        $result = $vm->process($this->request, $this->next);

        $this->assertInstanceOf('Psr\Http\Message\ResponseInterface', $result);
        $this->assertEquals($result, $response);
    }

    public function testProcess()
    {
        $this->request->shouldReceive('getAttribute')
            ->withArgs(['result'])
            ->andReturn(['content' => 'value']);

        $this->request->shouldReceive('getAttribute')
            ->withArgs(['route'])
            ->andReturn([
                'module' => 'test',
                'controller' => 'ctrl',
                'action' => 'act'
            ]);

        $this->request->shouldReceive('getAttribute')
            ->withArgs(['web_root'])
            ->andReturn('/');

        $response = new Response\TextResponse('hey ya !');
        $this->engine->shouldReceive('render')
            ->withArgs(['test/ctrl/act', ['webRoot' => '/', 'content' => 'value']])
            ->andReturn($response);

        $this->request->shouldReceive('withAttribute')
            ->withArgs(['response', $response])
            ->andReturn($this->request);

        $this->next->shouldReceive('handle')
            ->withArgs([$this->request])
            ->andReturn($response);

        $vm = new ViewMiddleware($this->engine);
        $result = $vm->process($this->request, $this->next);

        $this->assertInstanceOf('Psr\Http\Message\ResponseInterface', $result);
        $this->assertEquals($result, $response);
    }
}
