<?php

namespace Eco\ViewEngine\Test;

use Eco\ViewEngine\PhpEngine;
use PHPUnit\Framework\TestCase;

class PhpEngineTest extends TestCase
{

    public function testRender()
    {
        $viewEngine = new PhpEngine([
            'test' => __DIR__
        ]);

        $result = $viewEngine->render('test/template', ['content' => 'something']);

        $this->assertInstanceOf('Psr\Http\Message\MessageInterface', $result);
        $this->assertEquals(200, $result->getStatusCode());
        $this->assertContains('something', $result->getBody()->getContents());
    }

    public function testRenderWithError()
    {
        $this->expectException('\Exception');

        $viewEngine = new PhpEngine([
            'test' => __DIR__
        ]);

        $viewEngine->render('test/exception', []);
    }
}
